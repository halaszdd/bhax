#include <iostream>
#include <vector>
#include <cmath>
#include <numeric>
using namespace std;
bool prime(int n);
float sum(vector <float> &v);
int main()
{
    vector <int> primszamok;
    vector <int> ikerprimek;
    vector <float> reciprok;
    for(int i=1;i<1000000;i++)
    {
        if (prime(i))        
        {
            primszamok.push_back(i);
        }
    
    }
    for(int i=0;i<primszamok.size();i++)
    {
        for(int j=1;j<primszamok.size();j++)
        {
            if(primszamok[j]-primszamok[i]==2)
            {
                ikerprimek.push_back(primszamok[i]);
                ikerprimek.push_back(primszamok[j]);
            }
        }
    }
    for (int i=0;i<ikerprimek.size();i++)
    {
        reciprok.push_back(pow(ikerprimek[i],-1));
    }
    cout<<sum(reciprok)<<endl;
}
bool prime(int n)
{
    int i,db=0;
    for (i=1;i<=n;i++)
    {
        if (n%i==0)
            db++;

    }
    if (db==2)
        return true;
    return false;
}
float sum(vector <float> &v)
{
    float alap_ertek=0;
    return accumulate(v.begin(), v.end(), alap_ertek);
}