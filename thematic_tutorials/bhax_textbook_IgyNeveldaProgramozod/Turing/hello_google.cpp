#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;
//forrás https://progpater.blog.hu/2011/02/13/bearazzuk_a_masodik_labort
void rangok(double rangok[], int n)
{
    for (int  i = 0; i < n; i++)
    {
        cout<<rangok[i]<<endl;
    }
    
}
double tavolsag(double PR[], double PRv[], int n)
{
    double sum=0;
        for (int i = 0; i < n; i++)
        {
            sum+=( PRv[i]-PR[i])*(PRv[i]-PR[i]);
        }
        return sqrt(sum); //ez lesz a távolsága
}
void pgszamitas(double link[4][4])
{
    double PR[4]={0.0, 0.0, 0.0, 0.0};
    double alappr[4]={1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0,};
    for(;;)
    {
        for (int i = 0; i < 4; i++)
        {
                PR[i]=0;
                for (int j = 0; j < 4; j++)
                {
                        PR[i]+=link[i][j]*alappr[j];
                }
                
        }
        if (tavolsag(PR, alappr, 4)<0.0000000001)
        {
            break;
        }
        for (int i = 0; i < 4; i++)
        {
            alappr[i]=PR[i];
        }
    }
    rangok(PR,4);
}
int main()
{
    double linkdata_adott[4][4]=
    {
        {0.0,     0.0, 1.0/3.0, 0.0},
        {1.0, 1.0/2.0, 1.0/3.0, 1.0},
        {0.0, 1.0/2.0,     0.0, 0.0},
        {0.0,     0.0, 1.0/3.0, 0.0}
    };
    pgszamitas(linkdata_adott);
    return 0;
}