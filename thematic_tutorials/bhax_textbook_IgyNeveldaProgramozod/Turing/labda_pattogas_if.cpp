#include <curses.h>
#include <unistd.h>
using namespace std;
int main()
{
    WINDOW *console = initscr();
    int x=0;
    int y=0;
    int xinc=1;
    int yinc=1;
    int max_x;
    int max_y;

    for(;;)
    {
        getmaxyx(console, max_y, max_x); //valtoztatja a max_X/Y nagyságát
        mvprintw(y, x, "o");
        refresh();
        usleep(50000);
        clear();
        x+=xinc;
        y+=yinc;

        if(x>=max_x-1) //-1 mert a max ertekek sorszamozasa nem 0-tol hanem 1-tol kezdodik
            xinc*=-1;  //és egyebkent meg eltunik a labda ha nem vonuk le 1-t 
        if(x<=0)
            xinc*=-1;
        if(y>=max_y-1)
            yinc*=-1;
        if(y<=0)
            yinc*=-1;
    }
    return 0;
}