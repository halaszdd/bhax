#include <iostream>
#include <vector>
#include <cmath>
#include <string>

using namespace std;

template<typename T>

class node
{
public:
	node* bal = nullptr;
	node* jobb = nullptr;
	T value{};

	node() {}

	~node()
	{
		delete bal;
		delete jobb;
	}
	node(node<T>& other)
	{
		value = other.value;
		if(other.bal)
			bal = new node<T>(*other.bal);
		if(other.jobb)
			jobb = new node<T>(*other.jobb);
	}
	node(node<T>&& old)
	{
		value = old.value;
		std::swap(bal, old.bal); //bal=old.bal; old.bal=nullptr;
		std::swap(jobb,old.jobb);
	}

	void MakeRight()
	{
		if(!jobb)
			jobb = new node<T>;
	}
	void MakeLeft()
	{
		if(!bal)
			bal = new node<T>;
	}
	void print()
	{
		std::cout << value;
		if(bal)
			bal -> print();
		if(jobb)	
			jobb -> print();
	}
	void postorder()
	{
		if(bal)
			bal->postorder();
		if(jobb)
			jobb->postorder();
		//feldolgoz
	}
	void preorder()
	{
		//feldolgoz
		if(bal)
			bal->preorder();
		if(jobb)
			jobb->preorder();
	}
	void inorder()
	{
		if(bal)
			bal->inorder;
		//feldolgoz
		if(jobb)
			jobb->inorder;
	}
};

template <typename T>

class Binfa
{
	node<T>* root;

public:
	Binfa()
	{
		root = new node<T>;
	}
	~Binfa()
	{
		delete root;
	}
	Binfa(Binfa<T>& other)
	{
		root = new node<T>(*other.root);
	}
	Binfa(Binfa<T>&& old)
	{
		root = new node<T>(std::move(*old.root));
	}
	void insert(T elem)
	{
		node<T>* rootp = root;

		while (true)
		{
			if(rootp ->value < elem)
			{
				if(!rootp->jobb)
				{
					rootp -> MakeRight();
					rootp -> jobb -> value = elem;
					return;
				}
				else
				{
					rootp = rootp -> jobb;
				}
			}
			else
			{
				if(!rootp->bal)
				{
					rootp -> MakeLeft();
					rootp -> bal -> value = elem;
			Binfa<int> a;

	a.insert(3);

	Binfa<int> b(std::move(a));

	b.insert(5);
	b.insert(2);

	a.print();
	b.print();			return;
				}
				else
				{
					rootp = rootp -> bal;
				}
			}
		}
	}
	void print()
	{
		std::cout<<"Binary search tree: \n";
		root -> print();
	}
};

class LZWfa
{
	node<char>* root;

public:
	LZWfa() 
	{
		root = new node<char>;
	}

	~LZWfa()
	{
		delete root;
	}
	LZWfa(LZWfa& other)
	{
		root = new node<char>(*other.root);
	}
	LZWfa(LZWfa&& old)
	{
		root = new node<char>(std::move(*old.root));
	}

	LZWfa& operator=(LZWfa&& old)
	{
		std::swap(root, old.root);
		return *this;
	}

	unsigned int beillesztAg(const std::string& in, unsigned int startindex=0)
	{
		unsigned int index = startindex;
		char bit = -1;
		node<char>* akt = root;

		while(index < in.length())
		{
			bit = in[index];
			if (bit == '0')
				if (akt->bal)
					akt = akt->bal;
				else
				{
					akt->MakeLeft();
					akt->bal->value = '0';
					return index;		//kil�p, mert �j elemet kellett besz�rnia
				}
			else
				if (akt->jobb)
					akt = akt->jobb;
				else
				{
					akt->MakeRight();
					akt->jobb->value = '1';
					return index;		//kil�p, mert �j elemet kellett besz�rnia
				}
			index++;
		}
	}
	void beilleszt(const std::string& in)
	{
		unsigned int index = 0;
		while (index < in.length())
		{
			index += beillesztAg(in, index);
		}
	}
	void print()
	{
		std::cout<<"LZWfa: \n";
		root->print();
		std::cout<<"\n";
	}
};


int main()
{
	LZWfa a;
	a.beilleszt("010001");

	LZWfa copy(a);
	copy.beilleszt("11111");

	LZWfa move(std::move(copy));

	a.print();
	copy.print();
	move.print();
}