<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Someone?</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>                   

    <section>
        <title>SamuCam</title>
        <para>
            Mutassunk rá a webcam (pl. Androidos mobilod) kezelésére ebben a projektben:
        </para>
        <para>
            <link xlink:href="https://github.com/nbatfai/SamuCam">https://github.com/nbatfai/SamuCam</link>
        </para>
        <programlisting language="c++"><![CDATA[

SamuCam::SamuCam ( std::string videoStream, int width = 176, int height = 144 )
  : videoStream ( videoStream ), width ( width ), height ( height )
{
  openVideoStream();
}

...

void SamuCam::openVideoStream()
{
  videoCapture.open ( videoStream );

  videoCapture.set ( CV_CAP_PROP_FRAME_WIDTH, width );
  videoCapture.set ( CV_CAP_PROP_FRAME_HEIGHT, height );
  videoCapture.set ( CV_CAP_PROP_FPS, 10 );
}

void SamuCam::run()
{
    ...

  cv::Mat frame;

  while ( videoCapture.isOpened() )
    {

      QThread::msleep ( 50 );
      while ( videoCapture.read ( frame ) )
        {
            ...
        }

      if ( ! videoCapture.isOpened() )
        {
          openVideoStream();
        }
    }
}]]>
        </programlisting>
        <para>
            Lényegében a <link xlink:href="https://github.com/nbatfai/SamuCam/blob/master/SamuCam.cpp">SamuCam.cpp</link> fájlban használjuk az OpenCV-ből a <command>VideoCapture</command> osztályt, amivel tudunk kamerát kezelni, és a kamera felvételi módjának pontos részleteit is be tudjuk állítani vele ilyen például a felbontás, az fps is. A kódrészletben csak a kamerakezeléssel kapcsolatos dolgokat hagytam meg, így láthatjuk, hogy tényleg egyszerű így kezelni a kameránkat.
        </para>
        <para>
            További részletek az alábbi linken: <link xlink:href="https://docs.opencv.org/3.4/d8/dfe/classcv_1_1VideoCapture.html">https://docs.opencv.org/3.4/d8/dfe/classcv_1_1VideoCapture.html</link>
        </para>
    </section>

 <section>
        <title>EPAM: ASCII Art</title>
        <para>
            ASCII Art in Java! Implementálj egy Java parancssori programot, ami beolvas egy képet és kirajzolja azt a parancssorba és / vagy egy szöveges fájlba is ASCII karakterekkel.
        </para>
        <para>
            <programlisting language="java"><![CDATA[
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.io.File;
import java.io.FileOutputStream;
import javax.imageio.ImageIO;
import java.io.IOException;

public class AsciiPrinter {

    private static final char[] ASCII_PIXELS = { '$', '#', '*', ':', '.', ' ' };
    private static final byte[] NEW_LINE = "\n".getBytes();

    private OutputStream outputStream;
    private BufferedImage image;

    public AsciiPrinter(OutputStream outputStream, BufferedImage image) {
        this.outputStream = outputStream;
        this.image = image;
    }

    public void print() throws IOException {
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                outputStream.write(getAsciiChar(image.getRGB(j, i)));
            }
            outputStream.write(NEW_LINE);
        }
    }

    public static char getAsciiChar(int pixel) {
        return getAsciiCharFromGrayScale(getGreyScale(pixel));
    }

    public static int getGreyScale(int argb) {
        int red = (argb >> 16) & 0xff;
        int green = (argb >> 8) & 0xff;
        int blue = (argb) & 0xff;
        return (red + green + blue) / 3;
    }

    public static char getAsciiCharFromGrayScale(int greyScale) {
        return ASCII_PIXELS[greyScale / 51];
    }
    
    public static void main(String[] args) throws IOException {
        String imageName = args[0];
        String textFileName = null;
        OutputStream outputStream = System.out;
        if (args.length == 2){
            textFileName = args[1];
            outputStream = new FileOutputStream(textFileName);
        }
                
        BufferedImage image = ImageIO.read(new File(imageName));

        new AsciiPrinter(outputStream, image).print();
    }
}]]>
        </programlisting>
        </para>
        <figure>
            <title>Az első eredeti kép</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="prog2/someone/ascii-art/emoji_small.png" scale="40" />
                </imageobject>
            </mediaobject>
        </figure>
        <figure>
            <title>Az második eredeti kép</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="prog2/someone/ascii-art/thinking_small.png" scale="40" />
                </imageobject>
            </mediaobject>
        </figure>
        <para>
            Fordítás és használat (fájlnév nélkül konzolra megy a program!):
        </para>
        <screen><![CDATA[
javac AsciiPrinter.java
java AsciiPrinter emoji_small.png
java AsciiPrinter emoji_small.png test.txt
java AsciiPrinter <kép neve> <fájl neve>]]>
        </screen>
        <figure>
            <title>Az első ASCII rajzos kép</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="prog2/someone/ascii-art/ascii_emoji.png" scale="40" />
                </imageobject>
            </mediaobject>
        </figure>
        <figure>
            <title>Az második ASCII rajzos kép</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="prog2/someone/ascii-art/ascii_thinking.png" scale="40" />
                </imageobject>
            </mediaobject>
        </figure>  

    </section>

    <section>
        <title>EPAM: Titkos üzenet, száll a gépben!</title>
        <para>
            Implementájl egy olyan parancssori alkalmazást, amely a billentyűzetről olvas soronként ASCII karakterekből álló sorokat, és a beolvasott szöveget Caesar kódolással egy txt fájlba írja soronként.
        </para>
        <programlisting language="java"><![CDATA[
import java.io.*;
import java.util.Random;

public class CaesarCipher { //Osztálydefiníció
    public static String cipher(String message, int offset) { //messeage = lefordítandó szöveg, offset=eltolás nagysága az eredeti abc-hez képest
        StringBuilder result = new StringBuilder(); //builder használata a gyorsabb string összefűzésért
        for (char character : message.toCharArray()) {
            if (character != ' ') { //ha nem space, akkor titkosítjuk
                int originalAlphabetPosition = character - 'a'; //visszaadja a karakter távolságát az 'a' betűhöz képest (ascii) (abc-ben az elhelyezkedés)
                int newAlphabetPosition = (originalAlphabetPosition + offset) % 26; //26 itt az angol abc-hez van viszonyítva (A magyar ábécéra is működne, csak ott ugye az ASCII táblázat alapján máshol vannak, nem függnek egybe mint az angol abc betűi)
                char newCharacter = (char) ('a' + newAlphabetPosition); //visszaadjuk az 'a' ascii kódjához az abc-be mért távolságot (eltolással együtt)
                result.append(newCharacter); //titkosított karakter hozzáfűzése
            } else {
                result.append(character); //ha space, akkor marad space
            }
        }
        return result.toString(); //visszaadjuk a stringet yay
    }
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); //alap beolvasási hely a konzol
        BufferedWriter writer;
        int offset = 0;
        switch (args.length){
            case 1: { //ha egy argumentumunk van, akkor egy fájlba írjuk ki az eredményt, random offset-tel
                FileWriter file = new FileWriter(args[0]);
                writer = new BufferedWriter(file); //fájlba kiíratás
                offset = new Random().nextInt(25)+1; //random offset
                break;
            }
            case 2:{ //ha két argumentumunk van, akkor egy fájlba írjuk ki az eredményt, adott offsettel
                FileWriter file = new FileWriter(args[0]);
                writer = new BufferedWriter(file); //fájlba kiíratás
                offset = Integer.parseInt(args[1]); //offset beolvasása
                break;
            }
            default:{ //bármi más esetbe maradunk a default helyzetnél, a konzol a kimenet, és random offset
                writer = new BufferedWriter(new OutputStreamWriter(System.out)); //kimenet a konzol
                offset = new Random().nextInt(25)+1; //random offset
                break;
            }
        }
        String input = reader.readLine(); //beolvasunk egy sort
        while(input.length() != 0) { //itt mindig megnézzük, hogy a beolvasott sor 0-e, ha igen akkor kilépünk
            writer.write(cipher(input, offset)); //beolvasott sor
            writer.newLine(); //sorvége 
            input = reader.readLine();
        }
        reader.close(); //i'm insane :)
        writer.close();
    }
}]]>
        </programlisting>
    </section>
</chapter>     
