public class BinaryBubbleClass { //osztálydefiníció	
    private int data[]; //adattagok, tömb amibe le fogjuk tárolni a számokat
    private int actualSize = 0;//adattagok, indexelésre használjuk
    public BinaryBubbleClass(int size){ //konstruktor, létrehozzuk size méretű tömböt majd...
        if (size < 1) //nyílván 1től nagyobb tömb lesz majd ha ezt létrehozzuk
            System.out.println("Érténytelen kollekció méret!");
        else
            data = new int[size]; //létrehozzuk a size méretű adat tömbünket
    }
    public void add(int element) {
        //todo: Több elemet is lehessen hozzáadni!!!
        if(data.length - actualSize != 0) //max méret -  jelenlegi adatok száma, ha 0 akkor nincs hely
        {
            data[actualSize] = element;
            actualSize++;
        }
        else {
            System.out.println("Nincs több szabad hely a kollekcióba!");
        }
    }
    public int binarySearch(int kezdo, int veg, int keresett) {
        //Rendezett kollekcióra fusson!
        if (veg >= kezdo) { // ha nagyobb vagy = a vég a kezdő indexhez képest, akkor mehet a keresés, egyébként nincs ilyen elem (lásd: return -1)
            int kozepso = kezdo + (veg - kezdo) / 2;
            if (data[kozepso] == keresett)
                return kozepso; //a középső elemünk a keresett elemünk
            if (data[kozepso] > keresett)
                return binarySearch(kezdo, kozepso - 1, keresett); //balra keressük (kisebb mint a középső elem)
            return binarySearch(kozepso + 1, veg, keresett); //jobbra keressük (nagyobb mint a középső elem)
        }
        return -1; //-1 index nincs, így nincs az adatok közt a keresett szám
    }
    public void bubbleSort(){ //basic buborék rendezés, mindig a maximum kerül a végére, és onnastól jövünk vissza az elejéig
        for (int i = 0; i < actualSize - 1; i++) {
            for (int j = 0; j < actualSize - i - 1; j++) { //a vég felől indulunk visszafele a j-i miatt
                if (data[j] > data[j + 1]) { //szomszédok cseréje
                    int temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }
            }
        }
    }
    public int getActualSize(){
        return actualSize; //vissza adja a jelenleg eltárolt adatok számát
    }
    public void printValues(){ //szimpla kiíratása az adatoknak
        for (int x: data) { //for each
            System.out.print(x + " ");
        }
        System.out.print(System.lineSeparator()); //ez akár lehetne \n is
    }
    public static void main(String[] args) {
        BinaryBubbleClass test = new BinaryBubbleClass(5); //lepéldányosítjuk az osztályunkat
        test.add(17);
        test.add(4);
        test.add(94);
        test.add(12);
        test.add(24); //hozzáadunk x elemet
        System.out.println("Az eredeti értékek:");
        test.printValues(); //kiiratjuk az x elemeket eredeti sorrendben
        test.bubbleSort(); //buborék rendezést hajtunk végre az elemeken
        System.out.println("A rendezett értékek:");
        test.printValues(); //kiiratjuk az x elemeket rendezett sorrendben
        int keresett_elem = 94; //ez lesz a keresett elemünk
        int keresett_index = test.binarySearch(0, test.getActualSize(),keresett_elem); //fontos hogy itt a keresés során a keresett elem indexét fogjuk visszakapni!
        if(keresett_index != -1){ //-1 az index akkor, ha nincs ilyen elem (-1 nem index a tömböknél)
            System.out.println("A keresett elem " + keresett_elem +" indexe "+ keresett_index); //keresett elem és indexének kiiratása
        }
        else
            System.out.println("Nincs ilyen elem a kollekcióba!");
    }
}
