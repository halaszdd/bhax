import java.io.*;
import java.util.HashMap;
import java.util.Random;

public class LeetDicer {
    private HashMap<String, String[]> leets = new HashMap<>(); //Mapet használok mert kulcs-érték párokkal egyszerűbb megkeresni a szükséges értéket
	//				kulcs,  érték(ek)
	//				Character, String[]
    public LeetDicer() {
		//leets.put("kulcs", new String[] {kulcs1,kulcs2,})
        leets.put("a", new String[]{"4", "4", "@", "/-\\"});
        leets.put("b", new String[]{"b", "8", "|3", "|}"});
        leets.put("c", new String[]{"c", "(", "<", "{"});
        leets.put("d", new String[]{"d", "|)", "|]", "|}"});
        leets.put("e", new String[]{"3", "3", "3", "3"});
        leets.put("f", new String[]{"f", "|=", "ph", "|#"});
        leets.put("g", new String[]{"g", "6", "[", "[+"});
        leets.put("h", new String[]{"h", "4", "|-|", "[-]"});
        leets.put("i", new String[]{"1", "1", "|", "!"});
        leets.put("j", new String[]{"j", "7", "_|", "_/"});
        leets.put("k", new String[]{"k", "|<", "1<", "|{"});
        leets.put("l", new String[]{"l", "1", "|", "|_"});
        leets.put("m", new String[]{"m", "44", "(V)", "|\\/|"});
        leets.put("n", new String[]{"n", "|\\|", "/\\/", "/V"});
        leets.put("o", new String[]{"0", "0", "()", "[]"});
        leets.put("p", new String[]{"p", "/o", "|D", "|o"});
        leets.put("q", new String[]{"q", "9", "O_", "(,)"});
        leets.put("r", new String[]{"r", "12", "12", "|2"});
        leets.put("s", new String[]{"s", "5", "$", "$"});
        leets.put("t", new String[]{"t", "7", "7", "|"});
        leets.put("u", new String[]{"u", "|_|", "(_)", "[_]"});
        leets.put("v", new String[]{"v", "\\/", "\\/", "\\/"});
        leets.put("w", new String[]{"w", "VV", "\\/\\/", "(/\\)"});
        leets.put("x", new String[]{"x", "%", ")(", ")("});
        leets.put("y", new String[]{"y", "y", "y", "y"});
        leets.put("z", new String[]{"z", "2", "7_", ">_"});

        leets.put("0", new String[]{"D", "0", "D", "0"});
        leets.put("1", new String[]{"I", "I", "L", "L"});
        leets.put("2", new String[]{"Z", "Z", "Z", "e"});
        leets.put("3", new String[]{"E", "E", "E", "E"});
        leets.put("4", new String[]{"h", "h", "A", "A"});
        leets.put("5", new String[]{"S", "S", "S", "S"});
        leets.put("6", new String[]{"b", "b", "G", "G"});
        leets.put("7", new String[]{"T", "T", "j", "j"});
        leets.put("8", new String[]{"X", "X", "X", "X"});
        leets.put("9", new String[]{"g", "g", "j", "j"});
    }

    private String translateChar(char key) {
		//leets.get(key)
		//if (leets.get(key))
			//return leets.get(key)[new Random().nextInt(4)];
		//a map.get(kulcs) vissza adja a kulcshoz tartozó értéket
        if (leets.get(Character.toString(key)) != null) {
            return leets.get(Character.toString(key))[new Random().nextInt(4)];
        }
        return "";
    }

    public String toLeet(String text) { //lefordítjuk a kapott szöveget leetspeak-re
        StringBuilder builder = new StringBuilder(); //ez gyorsabb mint a stringek összefűzése (értsd string+='a'), főleg nagyobb szövegeknél
        for (char item : text.toCharArray()) { //ez itt egy foreach 
            String temp = translateChar(item);
            if (!temp.equals("")) {
                builder.append(temp); //hozzáadjuk a leet karaktereket
            } else {
                builder.append(item); //hozzáadjuk az eredeti karaktert, mert nincs leet megfelelője pl.: ékezetes karakterek
            }
        }
        return builder.toString(); //a builderből csinálunk egy stringet, értsd (kész)lefordított szöveg
    }

    public static void main(String[] args) throws IOException { //IOException a writer és a reader miatt van itt
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); //A konzolról olvasunk be (+lehetőség, akár lehetne fájlból is olvasni)
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out)); //A konzolra írunk be (hacsaaak...)
        if (args.length == 1) { //ha van egy argumentuma, akkor fájlba akarom kiíratni a lefordított szöveget és nem a konzolra
            FileWriter file = new FileWriter(args[0]); //fájl neve az args[0], itt létrehozzuk az üres fájlt
            writer = new BufferedWriter(file); //a fájlba fogunk kiírni
        }
        LeetDicer leeter = new LeetDicer(); //lepéldányosítjuk az osztályt
        String input = reader.readLine(); //beolvasunk egy sort
        while (input.length() != 0) { //addig csináljuk ameddig nem üres sorunk van
            writer.write(leeter.toLeet(input)); //átfordítjuk az eredeti szöveget leetspeak-re
            writer.newLine(); //új sorba kezdünk a fájlba/konzolon
            input = reader.readLine(); //beolvasunk egy sort
        }
        writer.close(); //lezárjuk a konzol kiíratást / fájlt
        reader.close(); //lezárjuk a beolvasó streamet (konzolt)
    }
}
//That's all folks!
