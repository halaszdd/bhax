public class PortScanner {
    public static void main(String[] args) { //az első argumentum egy ip (lehetne egy default is, de így is jó, pl: 127.0.0.1)
        for(int i=0; i<=65565; ++i) //portok 0-tól 65565-ig tartanak
            try {
                java.net.Socket socket = new java.net.Socket(args[0], i); //létrehozunk egy kapcsolatot egy ipvel és egy fix porttal, sikeresen fut le, ha van ilyen port amin él valami service, és dob egy Exception-t ha nem
                System.out.println(i + " figyeli"); //kiírjuk, hogy ezen a porton van kapcsolat az adott ip-n
                socket.close();  //Socket kapcsolat lezárása (Ez is egy Stream! (Closable, AutoClosable))
            } catch (Exception e) { //itt elkapjuk a dobott IOException-t amit a Socket dob, ha nem létezik ilyen ip-port páros 
                //System.out.println(i + " nem figyeli");
                //System.out.println(e.getClass().getSimpleName()); //IO Exception, mert nem tudjuk létrehozni a kapcsolatot ilyen porttal (Connection refused)
            }
    }
}

