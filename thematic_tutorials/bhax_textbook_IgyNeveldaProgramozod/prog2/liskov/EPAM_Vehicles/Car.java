public class Car extends Vehicle{ //A Vehicle-ből leörökli a dolgokat a Car
    @Override //felülírjuk a szülő start() metódusát a Gyerek osztályban
    public void start(){
        System.out.println("Starting a Car...");
    }
}