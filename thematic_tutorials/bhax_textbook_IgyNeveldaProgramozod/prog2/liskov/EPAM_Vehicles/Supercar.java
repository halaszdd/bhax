public class Supercar extends Car { //A Supercar szülője a Car
    @Override //felülírjuk a szülő start() metódusát a Gyerek osztályban
    public void start(){
        System.out.println("Starting a Supercar...");
    }
}