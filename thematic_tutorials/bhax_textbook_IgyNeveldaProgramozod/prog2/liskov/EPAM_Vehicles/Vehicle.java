public class Vehicle{ //Eredeti osztály
    public static void main (String[]args){
        Vehicle firstVehicle = new Supercar(); //Létrehozunk egy szülőt a Supercarral
        firstVehicle.start(); //Supercar start metódusa
        System.out.println(firstVehicle instanceof Car); //Kiírja, hogy a firstVehicle a Car osztálynak egy előfordulása/példánya
        Car secondVehicle = (Car) firstVehicle; //a firstVehicle-t átcastoljuk Car típusra "(Car)" (szükséges, hogy a két osztály között legyenek azonos tulajdonságok!)
        secondVehicle.start(); //Supercar start metódusa
        System.out.println(secondVehicle instanceof Supercar); //igaz, mert lényegében a firstVehicle-t használjuk itt is
        //Supercar thirdVehicle =  new Vehicle();
        //thirdVehicle.start();
    }
    public void start(){ //Vehicle metódusa
        System.out.println("Starting a Vehicle...");
    }
}