import java.io.*;
import java.util.Random;

public class CaesarCipher { //Osztálydefiníció
    public static String cipher(String message, int offset) { //messeage = lefordítandó szöveg, offset=eltolás nagysága az eredeti abc-hez képest
        StringBuilder result = new StringBuilder(); //builder használata a gyorsabb string összefűzésért
        for (char character : message.toCharArray()) {
            if (character != ' ') { //ha nem space, akkor titkosítjuk
                int originalAlphabetPosition = character - 'a'; //visszaadja a karakter távolságát az 'a' betűhöz képest (ascii) (abc-ben az elhelyezkedés)
                int newAlphabetPosition = (originalAlphabetPosition + offset) % 26; //26 itt az angol abc-hez van viszonyítva (A magyar ábécéra is működne, csak ott ugye az ASCII táblázat alapján máshol vannak, nem függnek egybe mint az angol abc betűi)
                char newCharacter = (char) ('a' + newAlphabetPosition); //visszaadjuk az 'a' ascii kódjához az abc-be mért távolságot (eltolással együtt)
                result.append(newCharacter); //titkosított karakter hozzáfűzése
            } else {
                result.append(character); //ha space, akkor marad space
            }
        }
        return result.toString(); //visszaadjuk a stringet yay
    }
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); //alap beolvasási hely a konzol
        BufferedWriter writer;
        int offset = 0;
        switch (args.length){
            case 1: { //ha egy argumentumunk van, akkor egy fájlba írjuk ki az eredményt, random offset-tel
                FileWriter file = new FileWriter(args[0]);
                writer = new BufferedWriter(file); //fájlba kiíratás
                offset = new Random().nextInt(25)+1; //random offset
                break;
            }
            case 2:{ //ha két argumentumunk van, akkor egy fájlba írjuk ki az eredményt, adott offsettel
                FileWriter file = new FileWriter(args[0]);
                writer = new BufferedWriter(file); //fájlba kiíratás
                offset = Integer.parseInt(args[1]); //offset beolvasása
                break;
            }
            default:{ //bármi más esetbe maradunk a default helyzetnél, a konzol a kimenet, és random offset
                writer = new BufferedWriter(new OutputStreamWriter(System.out)); //kimenet a konzol
                offset = new Random().nextInt(25)+1; //random offset
                break;
            }
        }
        String input = reader.readLine(); //beolvasunk egy sort
        while(input.length() != 0) { //itt mindig megnézzük, hogy a beolvasott sor 0-e, ha igen akkor kilépünk
            writer.write(cipher(input, offset)); //beolvasott sor
            writer.newLine(); //sorvége 
            input = reader.readLine();
        }
        reader.close(); //i'm insane :)
        writer.close();
    }
}
