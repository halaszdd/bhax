import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriterExample {
    private static class BugousStuffProducer {
        private final Writer writer; //final: értsd egyszer állíthatjuk be az értékét

        public BugousStuffProducer(String outputFileName) throws IOException {
            writer = new FileWriter(outputFileName);
        }

        public void writeStuff() throws IOException {
            writer.write("Stuff");
        }

        @Override
        public void finalize() throws IOException {
            writer.close();
        }
    }
    
    public static void main(String[] args) throws IOException {
        BugousStuffProducer stuffProducer = new BugousStuffProducer("something.txt"); 
        stuffProducer.writeStuff();
        System.gc(); //így hívjuk meg a Garbage Collector-t manuálisan
    }
}
