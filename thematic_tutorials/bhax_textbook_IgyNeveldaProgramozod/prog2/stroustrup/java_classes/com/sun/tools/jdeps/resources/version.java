package com.sun.tools.jdeps.resources;

public final class version extends java.util.ListResourceBundle {
    protected final Object[][] getContents() {
        return new Object[][] {
            { "full", "1.8.0_275-8u275-b01-0ubuntu1~18.04-b01" },
            { "jdk", "1.8.0_275" },
            { "release", "1.8.0_275" },
        };
    }
}
