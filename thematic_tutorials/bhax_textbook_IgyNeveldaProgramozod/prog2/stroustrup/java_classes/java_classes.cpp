// g++ java_classes.cpp -o java_classes -lboost_system -lboost_filesystem
// ./java_classes

// !!! Fontos hogy a program futtatása előtt tömörítsük ki neki az src.zip-ben található fájlokat !!!
// sudo apt install openjdk-8-source
// cp /usr/lib/jvm/openjdk-8/src.zip .
// unzip src.zip
// vagy valami más úton módon (Windows imádunk...)

#include <iostream> 
#include <vector>

#include <boost/foreach.hpp> //boost import: foreach miatt
#include <boost/filesystem.hpp>//boost import: útvonal kezelés és fájlkezelés miatt 
#include <boost/xpressive/regex_actions.hpp>//boost import: nemtudom mihez?

using namespace std;
int main(){
    vector<string> classes; //Ebbe fogom tárolni a fájlneveket, mert így egyszerűbb összeszámolni, és kiíratni ha szükség lenne rá a későbbiekben
    boost::filesystem::path targetDir("./src/"); //a programhoz képest ugyan abban a mappában az src mappa tartalmai közt fogunk járkálni

    boost::filesystem::recursive_directory_iterator iter(targetDir), eod;

    BOOST_FOREACH(boost::filesystem::path const& i, make_pair(iter, eod))
    {
		//ugye egy java osztályt akkor lelünk a források közt, ha .java végződésű fájlunk van
		//(lehetne tovább bontani intefészre, meg absztrakt osztályra, de akkor már bele kéne olvasni a fájlba, amit itt nem akarunk)
        if (is_regular_file(i) && i.filename().string().find(".java") != string::npos){ //string::npos = -1 !!!!!!!!!!!!
            cout << i.filename().string() << endl;
            classes.push_back(i.filename().string());
        }
    }
    cout << "===========================================" << endl; //a sok fájlnév után kiiratok egy vonalat hogy lássam honnan jön a szám
    cout << classes.size() << " db java osztály van az src.zip-ben"<< endl; //classes.size megegyezik a .java-ra végződő fájlok számával
}
