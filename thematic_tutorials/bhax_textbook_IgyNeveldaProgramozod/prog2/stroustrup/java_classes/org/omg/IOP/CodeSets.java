package org.omg.IOP;


/**
* org/omg/IOP/CodeSets.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from /build/openjdk-8-ElpxQ7/openjdk-8-8u275-b01/src/corba/src/share/classes/org/omg/PortableInterceptor/IOP.idl
* Monday, November 9, 2020 3:43:17 AM UTC
*/

public interface CodeSets
{

  /**
       * Identifies a CDR encapsulation of the 
       * <code>CONV_FRAME.CodeSetContext</code> defined in 
       * Section 13.10.2.5, "GIOP Code Set Service Context," on page 13-43.
       */
  public static final int value = (int)(1L);
}
