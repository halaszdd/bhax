package org.omg.PortableInterceptor;


/**
* org/omg/PortableInterceptor/HOLDING.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from /build/openjdk-8-ElpxQ7/openjdk-8-8u275-b01/src/corba/src/share/classes/org/omg/PortableInterceptor/Interceptors.idl
* Monday, November 9, 2020 3:43:17 AM UTC
*/

public interface HOLDING
{

  /** Object adapter state that holds requests temporarily until the 
     * state is changed.
     */
  public static final short value = (short)(0);
}
