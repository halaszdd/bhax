package org.omg.PortableServer;


/**
* org/omg/PortableServer/REQUEST_PROCESSING_POLICY_ID.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from /build/openjdk-8-ElpxQ7/openjdk-8-8u275-b01/src/corba/src/share/classes/org/omg/PortableServer/poa.idl
* Monday, November 9, 2020 3:43:17 AM UTC
*/

public interface REQUEST_PROCESSING_POLICY_ID
{

  /**
  	 * The value representing REQUEST_PROCESSING_POLICY_ID.
  	 */
  public static final int value = (int)(22L);
}
